import { FbPage } from './app.po';

describe('fb App', () => {
  let page: FbPage;

  beforeEach(() => {
    page = new FbPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
